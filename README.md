# Fusilli-Kore Helm Chart
Helm chart used to install the fusilli-kore

# Install Helm
To install **Helm** type in terminal the following commands
```bash
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```
Verify your installation 
```bash
helm version
```

# Install fusilli-kore

In order to install the fusilli-kore you will need to customize the file **config.yml** by changing the image url that you need to install.
As you modified the **config.yml** file type the following to verify that your changes inside the config file are loaded correctly 
```bash
helm install [application name] [folder|repo name] -f config.yml --dry-run --debug
```
Now to install the fusilli-kore type the following command
```bash
helm install fusilli-kore fusilli.io-helm
```
Is also possible to pass the value of the parameter used in config file trough the **helm install** command
```bash
helm install fusilli-kore fusilli.io-helm --set replicaCount=2
```
List of the parameter

|PARAMETER                |DESCRIPTION                          |DEFAULT                         |
|:-----------------|:-------------------------------------------------|:-----------------------------|
|configMap.data |Environment variable to source a configuration file|null |
|secret.stringData|Environment variable that store the play secret key|null |
|service.type|Service type to expose your application|LoadBalancer |
|service.port|Port of the service external ip|8081 |
|service.protocol|Protocol used by the service|TCP |
|replicaCount|Number of replicas to install|2 |
|image.repository|Image url |null |
|image.tag|Image version |null |
|image.pullPolicy|Policy to pull the image from the repository|IfNotPresent|
